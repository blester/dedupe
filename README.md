Dedupe, File Deduplication Assistant
====================================

Dedupe is a simple, open source file deduplication utility written in Java using Swing.


Command-Line Build
------------------

```
gradle buildVersion -Pv=x.x.x
```

Official Website
----------------

[http://dedupe.bretlester.com](http://dedupe.bretlester.com)


