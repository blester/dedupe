package com.bretlester.dedupe;

import com.threealike.life.file.Get;
import com.threealike.life.file.Put;
import com.threealike.life.thirdparty.org.json.JSONArray;
import com.threealike.life.thirdparty.org.json.JSONObject;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.ArrayList;
import java.util.List;

public class Scanjob extends Thread {

    private final Dir dir;
    private final ScanHandler scanHandler;
    private boolean canceled = false;

    public Scanjob(Dir dir, ScanHandler scanHandler) {
        this.dir = dir;
        this.scanHandler = scanHandler;
    }

    boolean isIgnored(Path path) {
        JSONArray ignores = dir.getIgnores();

        if(com.threealike.life.util.Files.canonicalPath(path.toFile()).equals(dir.getPath()))
            return false;

        if(".Dedupe".equals(path.getFileName().toString()))
            return true;

        for(int i=0; i < ignores.length(); i++) {
            JSONObject ignore = ignores.getJSONObject(i);
            String patt = ignore.getString("pat");
            Boolean full = ignore.getBoolean("full");
            System.out.println(patt+" : "+full);
            String compare = full ?
                    com.threealike.life.util.Files.relativePath(dir.getFile(), path.toFile())
                    : path.getName(path.getNameCount()-1).toString();
            if(compare.matches(patt))
                return true;
        }
        return false;
    }

    private String getFileId(File f) {
        File idFile = refPath(f);
        String id;
        if(!idFile.exists()) {
            id = com.threealike.life.util.Files.md5Of(f);
            idFile.getParentFile().mkdirs();
            com.threealike.life.util.Files.create(idFile);
            Put.contents(idFile, id);
        } else {
            id = com.threealike.life.util.Files.getContents(idFile);
        }
        return id;
    }

    @Override
    public void run() {

        final Scanjob job = this;
        final Util.TypeBox totalFiles = new Util.TypeBox();
        totalFiles.intVal = 0;
        final Util.TypeBox currentFile = new Util.TypeBox();
        currentFile.intVal = 0;

        scanHandler.beforeStart();

        try {

            Files.walkFileTree(new File(dir.getPath()).toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {

                    if (canceled) {
                        scanHandler.onFinish();
                        return FileVisitResult.TERMINATE;
                    }

                    if(isIgnored(dir))
                        return FileVisitResult.SKIP_SUBTREE;

                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

                    File f = file.toFile();

                    if(f.isDirectory())
                        return FileVisitResult.CONTINUE;

                    if (canceled) {
                        scanHandler.onFinish();
                        return FileVisitResult.TERMINATE;
                    }

                    if(isIgnored(file)) {
                        String id = getFileId(f);
                        File dups = indexPath(dir, id);
                        if(dups.exists())
                            dups.delete();
                        return FileVisitResult.CONTINUE;
                    }

                    totalFiles.intVal++;
                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException ex) {
                    return FileVisitResult.SKIP_SUBTREE;
                }
            });

            Files.walkFileTree(new File(dir.getPath()).toPath(), new SimpleFileVisitor<Path>() {
                @Override
                public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attrs) {

                    if (canceled) {
                        scanHandler.onFinish();
                        return FileVisitResult.TERMINATE;
                    }

                    if(isIgnored(dir))
                        return FileVisitResult.SKIP_SUBTREE;

                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
                    File f = file.toFile();

                    if(f.isDirectory())
                        return FileVisitResult.CONTINUE;

                    if(isIgnored(file))
                        return FileVisitResult.CONTINUE;

                    currentFile.intVal++;
                    scanHandler.onVisitFile(job, file, currentFile.intVal, totalFiles.intVal);
                    if (canceled) {
                        scanHandler.onFinish();
                        return FileVisitResult.TERMINATE;
                    }

                    String id = getFileId(f);

                    registerDup(id, f);

                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult postVisitDirectory(Path scandir, IOException e) {

                    if (com.threealike.life.util.Files.canonicalPath(scandir.toFile()).equals(dir.getPath())) {
                        scanHandler.onFinish();
                    }

                    return FileVisitResult.CONTINUE;
                }
                @Override
                public FileVisitResult visitFileFailed(Path file, IOException ex) {
                    return FileVisitResult.SKIP_SUBTREE;
                }
            });
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }



    private void registerDup(String id, File f) {
        File indexFile = indexPath(dir, id);
        if(indexFile.exists()) {
            String[] dups = com.threealike.life.util.Files.getContents(indexFile).split("\n");
            int i;
            for(i=0; i < dups.length; i++) {
                if(dups[i] == null || dups[i].equals("")) continue;
                if(dups[i].equals(com.threealike.life.util.Files.canonicalPath(f))) {
                    if(i > 0) notifyDupe(id);
                    return;
                }
            }
            if(i > 0) notifyDupe(id);
        } else {
            indexFile.getParentFile().mkdirs();
            com.threealike.life.util.Files.create(indexFile);
        }

        com.threealike.life.util.Files.append(indexFile, com.threealike.life.util.Files.canonicalPath(f)+"\n");
    }

    private void notifyDupe(String id) {

        File dupeFile = new File(dirBase(dir), "dups/"+id);
        dupeFile.getParentFile().mkdirs();
        com.threealike.life.util.Files.create(dupeFile);
        scanHandler.onFoundDupe();
    }

    public static File dirBase(Dir dir) {
        return new File(dir.getFile(), ".Dedupe");
    }

    public static File refBase(Dir dir) {
        File out = new File(dirBase(dir), "ref");
        return out;
    }

    private File refPath(File f) {
        String rp = com.threealike.life.util.Files.relativePath(dir.getFile(), f);
        File out = new File(refBase(dir), rp+".txt");
        return out;
    }

    public static File indexBase(Dir dir) {
        File out = new File(dirBase(dir), "index");
        out.mkdirs();
        return out;
    }

    public static File indexPath(Dir dir, String md5) {
        String path = "";
        for(int i=0; i < md5.length(); i+=2) {
            path += "_"+md5.substring(i, i+2)+"/";
        }
        return new File(indexBase(dir), path+"dups.txt");
    }

    public void cancel() {
        canceled = true;
    }

    private void pause(long ms) {
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static abstract class ScanHandler {
        void beforeStart() {};
        abstract void onVisitFile(Scanjob job, Path p, int currentFile, int totalFiles);
        abstract void onFinish();
        abstract boolean isCanceled();
        void onFoundDupe() {}
    }
}
