package com.bretlester.dedupe;

import com.threealike.life.util.Files;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import static com.bretlester.dedupe.Util.getString;

public class PickerDialog extends JDialog {
    public PickerDialog(JFrame frame, final Dir selectedDir, final String[] dups, final WalkResult result) {
        super(frame);

        final JDialog dialog = this;
        final JButton deleteBtn = new JButton(getString("Delete"));
        final JButton skipButton = new JButton(getString("Skip"));
        final JCheckBox folderOption = new JCheckBox(" ");

        dialog.setModal(true);
        dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        dialog.setTitle(getString("Delete Duplicates"));

        GridBagConstraints c = new GridBagConstraints();

        JPanel pane = new JPanel(new GridBagLayout());
        pane.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        JLabel label = new JLabel(getString("SelectDup"));
        c.anchor = GridBagConstraints.PAGE_START;
        c.gridy = 0;
        c.weighty = .25;
        c.gridx = 0;
        c.gridwidth = 3;
        c.weightx = 1;
        pane.add(label, c);

        JPanel selectionPane = new JPanel(new GridBagLayout());

        final JCheckBox[] selection = new JCheckBox[dups.length];
        for(int i=0; i < dups.length; i++) {
            final String dup = dups[i];
            final int index = i;

            c.weighty = 0;
            c.anchor = GridBagConstraints.LINE_START;
            c.gridy = i;
            c.gridx = 0;
            c.gridwidth = 1;
            c.weightx = .5;
            final JCheckBox cb = selection[i] = new JCheckBox(
                    Files.relativePath(selectedDir.getFile(), new File(dups[i])));
            cb.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    if(cb.isSelected()) {
                        for(int i=0; i < selection.length; i++) {
                            if(i == index) continue;
                            selection[i].setSelected(false);
                        }
                        File dupFile = new File(dup);
                        if(!Files.equal(dupFile.getParentFile(), selectedDir.getFile())) {
                            String rp = Files.relativePath(selectedDir.getFile(), dupFile.getParentFile());
                            folderOption.setEnabled(true);
                            folderOption.setText("Always delete duplicates in "+rp);
                        }
                        deleteBtn.setEnabled(true);
                    } else {
                        deleteBtn.setEnabled(false);
                        folderOption.setEnabled(false);
                        folderOption.setText(" ");
                    }
                }
            });
            selectionPane.add(cb, c);

            c.gridx = 1;
            c.anchor = GridBagConstraints.LINE_END;
            c.gridwidth = 1;
            JButton button = new JButton();
            button.setText("<html><font color=\"#000099\">"+getString("OpenContaining")+"</font></html>");
            button.setBorderPainted(false);
            button.setOpaque(false);
            button.setBackground(Color.WHITE);
            button.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    try {
                        Desktop.getDesktop().open(new File(dup).getParentFile());
                    } catch (IOException e1) {
                        throw new RuntimeException(e1);
                    }
                }
            });
            selectionPane.add(button, c);
        }
        c.weighty = .40;
        c.gridy = 1;
        c.gridx = 0;
        c.gridwidth = 3;
        c.weightx = 1;
        c.anchor = GridBagConstraints.CENTER;
        pane.add(selectionPane, c);

        GridBagConstraints c1 = new GridBagConstraints();
        JPanel opt = new JPanel(new GridBagLayout());
        folderOption.setEnabled(false);
        opt.setBorder(BorderFactory.createLoweredBevelBorder());
        c1.anchor = GridBagConstraints.FIRST_LINE_START;
        c1.gridy = 0;
        c1.gridx = 0;
        c1.weightx = 1;
        c1.ipady = 10;
        opt.add(folderOption, c1);
        c.gridy = 2;
        c.weighty = .1;
        c.fill = GridBagConstraints.HORIZONTAL;
        c.insets = new Insets(10,10,10,10);
        pane.add(opt, c);

        JButton cancelButton = new JButton(getString("Cancel"));
        c.gridx = 0;
        c.gridy = 3;
        c.weighty = .25;
        c.gridwidth = 1;
        c.anchor = GridBagConstraints.LINE_END;
        c.insets = new Insets(0,0,0,2);
        c.weightx = 1;
        pane.add(cancelButton, c);
        cancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                result.cancel = true;
                dialog.dispose();
            }
        });

        deleteBtn.setEnabled(false);
        c.gridx = 1;
        c.anchor = GridBagConstraints.CENTER;
        c.insets = new Insets(0, 2, 0, 2);
        c.weightx = 0;
        pane.add(deleteBtn, c);
        deleteBtn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                for(int i=0; i < selection.length; i++) {
                    if(selection[i].isSelected()) {
                        if(folderOption.isSelected())
                            result.deleteAllFilesInParent = true;
                        result.delete = new File(dups[i]);
                        dialog.dispose();
                        return;
                    }
                }

            }
        });

        c.gridx = 2;
        c.anchor = GridBagConstraints.LINE_START;
        c.insets = new Insets(0, 2, 0, 0);
        c.weightx = 1;
        pane.add(skipButton, c);
        skipButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                dialog.dispose();
                result.skip = true;
            }
        });

        dialog.add(pane);
        dialog.pack();
        dialog.setLocationRelativeTo(frame);
        fit(dialog);



    }

    private void fit(final JDialog dialog) {
        if(dialog.getWidth() < 300)
            dialog.setSize(300, dialog.getHeight());
        if(dialog.getHeight() < 200)
            dialog.setSize(dialog.getWidth(), 200);
    }

    public static class WalkResult {
        Boolean skip;
        Boolean cancel;
        Boolean cancelSkip;
        File delete;
        boolean deleteAllFilesInParent = false;
        boolean haveResults() {
            return skip != null || cancel != null || delete != null || cancelSkip != null;
        }
    }
}
