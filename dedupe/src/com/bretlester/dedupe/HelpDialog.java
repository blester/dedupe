package com.bretlester.dedupe;

import javax.swing.*;

import java.awt.*;
import java.io.IOException;
import java.util.Properties;

import static com.bretlester.dedupe.Util.getString;


public class HelpDialog extends JDialog {
    public HelpDialog(JFrame frame) {
        super(frame);

        this.setModal(true);
        this.setTitle(getString("About"));

        GridBagConstraints c = new GridBagConstraints();
        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));

        JLabel titleLabel = new JLabel("Dedupe by Bret Lester");
        c.weightx = 1;
        c.gridy = 0;
        c.ipadx = 10;
        c.ipady = 10;
        panel.add(titleLabel, c);

        JLabel vLabel = new JLabel(getString("Version")+" "+version());
        c.gridy = 1;
        panel.add(vLabel, c);

        JLabel siteLabel = new JLabel(("http://dedupe.bretlester.com"));
        c.gridy = 2;
        panel.add(siteLabel, c);

        add(panel);

        pack();
    }

    String version() {
        Properties props = new Properties();
        try {
            props.load(ClassLoader.getSystemResourceAsStream("cfg.properties"));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        return props.getProperty("dedupe.version");

    }

}
