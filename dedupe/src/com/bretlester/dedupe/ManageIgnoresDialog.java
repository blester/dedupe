package com.bretlester.dedupe;


import com.threealike.life.thirdparty.org.json.JSONArray;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import static com.bretlester.dedupe.Util.getString;

public class ManageIgnoresDialog extends JDialog {

    private final Dir selectedDir;
    private final List<Dir> dirs;
    private final JTable table;
    private final DefaultTableModel tableModel;
    private final JMenuItem deleteOption;

    public ManageIgnoresDialog(JFrame frame, Dir selectedDir, List<Dir> dirs) {
        super(frame);

        this.selectedDir = selectedDir;
        this.dirs = dirs;

        this.setModal(true);
        this.setTitle(getString("ManageIgnores"));

        JMenuBar menuBar = new JMenuBar();
        JMenu menu = new JMenu(getString("Option"));
        menu.add(addIgnoreMenuItem());
        menu.add(deleteOption=addDeleteMenuItem());
        menuBar.add(menu);

        this.setJMenuBar(menuBar);

        table = initTable(tableModel = initTableModel());
        JScrollPane scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);
        this.add(scrollPane);

        this.setSize(300, 200);

        this.setLocationRelativeTo(frame);
    }

    private JTable initTable(DefaultTableModel model) {
        JTable out = new JTable(model) {
            @Override
            public Class getColumnClass(int column) {
                if(column == 1)
                    return Boolean.class;
                return super.getColumnClass(column);
            }
        };
        out.getSelectionModel().addListSelectionListener(new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel lsm = (ListSelectionModel)e.getSource();

                deleteOption.setEnabled(false);

                if((!lsm.getValueIsAdjusting()) && table.getSelectedRow() != -1
                        && lsm.getMaxSelectionIndex() == lsm.getMinSelectionIndex()) {
                    deleteOption.setEnabled(true);
                }
            }
        });
        return out;
    }

    private DefaultTableModel initTableModel() {
        DefaultTableModel out = new DefaultTableModel(
                getIgnores(),
                new String[] {
                        getString("IgnorePattern"),
                        getString("MatchFullPath")
                }
        ) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return column == 1;
            }
        };
        out.addTableModelListener(new TableModelListener() {
            @Override
            public void tableChanged(TableModelEvent e) {
                TableModel source = (TableModel)e.getSource();
                if(e.getFirstRow() == -1 || e.getColumn() == -1) return;
                Boolean data = (Boolean)tableModel.getValueAt(e.getFirstRow(), e.getColumn());
                selectedDir.getIgnores().getJSONObject(e.getFirstRow()).put("full", data);
                DAO.INST.serialize(dirs);
            }
        });
        return out;
    }

    Object[][] getIgnores() {
        JSONArray ignores = selectedDir.getIgnores();
        Object[][] out = new Object[ignores.length()][];
        for(int i=0; i < out.length; i++) {
            out[i] = new Object[]{
                    ignores.getJSONObject(i).getString("pat"),
                    ignores.getJSONObject(i).optBoolean("full")
            };
        }
        return out;
    }

    JMenuItem addDeleteMenuItem() {
        JMenuItem deleteOption = new JMenuItem(getString("Delete"));
        deleteOption.setEnabled(false);
        final JDialog ctx = this;
        deleteOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int choice = JOptionPane.showConfirmDialog(ctx, getString("DeleteConfirm"), null, JOptionPane.YES_NO_OPTION);
                if(choice != 0) return;
                int index = table.getSelectedRow();
                selectedDir.deleteIgnoreAt(index);
                DAO.INST.serialize(dirs);
                tableModel.removeRow(index);
            }
        });
        return deleteOption;
    }

    JMenuItem addIgnoreMenuItem() {
        JMenuItem addOption = new JMenuItem(getString("AddIgnorePattern"));
        addOption.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String patt = JOptionPane.showInputDialog(null, getString("AddIgnorePattern"));
                if(patt != null && (!(patt=patt.trim()).equals(""))) {

                    selectedDir.addIgnore(patt, false);
                    DAO.INST.serialize(dirs);
                    tableModel.addRow(new Object[] {
                            patt, false
                    });
                }
            }
        });
        return addOption;
    }

}

