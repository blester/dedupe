package com.bretlester.dedupe;

import java.util.HashMap;
import java.util.Map;

public class  Util {
    private static Map<String, String> strings = new HashMap<>();
    static {
        strings.put("File", "File");
        strings.put("Files", "Files");
        strings.put("AddDirectory", "Add Directory");
        strings.put("Path", "Path");
        strings.put("FileCount", "File Count");
        strings.put("Size", "Size");
        strings.put("Status", "Status");
        strings.put("unknown", "unknown");
        strings.put("idle", "idle");
        strings.put("Directories", "Directories");
        strings.put("Action", "Action");
        strings.put("Analyze", "Analyze");
        strings.put("FilesAnalyzed", "Files Analyzed");
        strings.put("Analyzing", "Analyzing");
        strings.put("Delete", "Delete");
        strings.put("DeleteConfirm", "Are you sure you want to delete that?");
        strings.put("Directory", "Directory");
        strings.put("Analyzing Directory", "Analyzing Directory");
        strings.put("Cancel", "Cancel");
        strings.put("Reset", "Reset");
        strings.put("Clean", "Clean");
        strings.put("Cancelling", "Cancelling");
        strings.put("Cleaning", "Cleaning");
        strings.put("Cleaning Directory", "Cleaning Directory");
        strings.put("Duplicates", "Duplicates");
        strings.put("Delete Duplicates", "Delete Duplicates");
        strings.put("unknown", "unknown");
        strings.put("Completed", "Completed");
        strings.put("Open", "Open");
        strings.put("Skip", "Skip");
        strings.put("SelectDup", "Select which duplicate to delete");
        strings.put("OK", "OK");
        strings.put("DeleteFail", "I failed to delete that file");
        strings.put("OpenContaining", "Open Containing Folder");
        strings.put("ManageIgnores", "Manage Ignores");
        strings.put("Folder", "Folder");
        strings.put("IgnorePattern", "Ignore Pattern");
        strings.put("Options", "Options");
        strings.put("AddIgnorePattern", "Add Ignore Pattern");
        strings.put("Option", "Option");
        strings.put("MatchFullPath", "Match Full Path");
        strings.put("Help", "Help");
        strings.put("About", "About");
        strings.put("Version", "Version");
    }

    public static String getString(String key) {

        if(strings.get(key) == null)
            throw new RuntimeException("Couldn't find key, \""+key+"\"");

        return strings.get(key);

    }

    public static class TypeBox {
        public int intVal;
        public long longVal;
        public String stringVal;
        public boolean booleanVal;
        public long[] arrayLongVal;

        public TypeBox() {}

        public TypeBox(long[] arrayLongVal) {
            this.arrayLongVal = arrayLongVal;
        }

        public TypeBox(long val) {
            longVal = val;
        }

        public TypeBox(boolean val) {
            booleanVal = val;
        }
    }

}
