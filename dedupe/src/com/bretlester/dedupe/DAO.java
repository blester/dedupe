package com.bretlester.dedupe;


import com.threealike.life.file.Put;
import com.threealike.life.thirdparty.org.json.JSONArray;
import com.threealike.life.util.Files;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

public enum DAO {
    INST;

    public List<Dir> getDirs() {
        List<Dir> out = new ArrayList<>();

        JSONArray ja = settingsJSON();
        for(int i=0; i < ja.length(); i++) {
            out.add(new Dir(ja.getJSONObject(i)));
        }

        return out;
    }

    public boolean addDir(File dir) {
        Dir newDir = new Dir(dir);
        if(!Scanjob.dirBase(newDir).mkdir())
            return false;
        List<Dir> dirs = getDirs();
        dirs.add(newDir);
        serialize(dirs);
        return true;
    }

    public void serialize(List<Dir> dirs) {
        JSONArray ja = new JSONArray();
        for(int i=0; i < dirs.size(); i++) {
            ja.put(dirs.get(i).toJSON());
        }
        Put.contents(settingsFile(), ja.toString());
    }

    private JSONArray settingsJSON() {
        try {
            return new JSONArray(settingsFile());
        } catch (FileNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public File settingsFolder() {
        File settingsFolder = new File(homeDir(), ".Dedupe");
        settingsFolder.mkdir();
        return settingsFolder;
    }

    public File settingsFile() {

        File settingsFolder = settingsFolder();
        File dirsFile = new File(settingsFolder, "dirs.json");
        if(!dirsFile.exists()) {
            Files.create(dirsFile);
            Put.contents(dirsFile, "[]");
        }
        return dirsFile;
    }

    private File homeDir() {
        return new File(System.getProperty("user.home"));
    }
}
