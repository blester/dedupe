package com.bretlester.dedupe;


import com.threealike.life.thirdparty.org.json.JSONArray;
import com.threealike.life.thirdparty.org.json.JSONObject;
import com.threealike.life.util.Files;
import com.threealike.life.util.Strings;

import java.io.File;
import java.util.List;

public class Dir {

    private final File dir;
    private String id;
    private int duplicates = -1;
    private int fileCount = -1;
    private int analyzed = 0;
    private JSONArray ignores = new JSONArray().put(
            new JSONObject().put("pat", "^\\..*").put("full", false));

    public Dir(JSONObject jo) {
        this.dir = new File(jo.getString("path"));
        this.duplicates = jo.optInt("duplicates");
        this.fileCount = jo.optInt("files");
        this.analyzed = jo.optInt("analyzed");
        this.ignores = jo.optJSONArray("ignores") == null ? new JSONArray() : jo.optJSONArray("ignores");
    }

    public Dir(File dir) {
        this.dir = dir;
    }

    public JSONObject toJSON() {
        return new JSONObject()
                .put("path", Files.canonicalPath(dir))
                .put("id", getId())
                .put("duplicates", duplicates)
                .put("files", fileCount)
                .put("analyzed", analyzed)
                .put("ignores", ignores)
                ;
    }

    public File getFile() {
        return dir;
    }

    public String getPath() {
        return Files.canonicalPath(dir);
    }

    public JSONArray getIgnores() {
        return ignores;
    }

    public void addIgnore(String pat, boolean full) {
        ignores.put(new JSONObject()
            .put("pat", pat)
                .put("full", full)
        );
    }

    public void deleteIgnoreAt(int i) {
        JSONArray ignores = getIgnores();
        if(ignores.length() > i)
            ignores.remove(i);
    }

    public String getId() {
        if(id == null)
            id = Strings.sha256Of(getPath());
        return id;
    }

    public int getDuplicates() {
        return duplicates;
    }

    public void setDuplicates(int dups) {
        this.duplicates = dups;
    }

    public int getFileCount() {
        return fileCount;
    }

    public void setFileCount(int cnt) {
        fileCount = cnt;
    }

    public void setAnalyzed(int n) {
        analyzed = n;
    }

    public int getAnalyzed() {
        return analyzed;
    }
}
