package com.bretlester.dedupe;

import com.threealike.life.file.Put;
import com.threealike.life.util.Files;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.*;
import java.util.List;

import static com.bretlester.dedupe.Util.getString;

/**
 * Created by blester on 5/16/14.
 *
 * TODO: don't allow addition of multiple of the same direcotry
 */
public class Dedupe {

    private static JFrame frame;

    /**
     * Create the GUI and show it.  For thread safety,
     * this method should be invoked from the
     * event-dispatching thread.
     */
    private static void createAndShowGUI() {
        //Create and set up the window.
        JMenuBar menuBar;
        JMenu menu;
        final JMenu actionMenu = new JMenu(getString("Option"));
        JMenuItem menuItem;
        JScrollPane scrollPane;
        final DefaultTableModel tableModel = new DefaultTableModel(new Object[][]{}, new String[]{
                getString("Path"),
                getString("Files"),
                getString("Duplicates"),
                getString("FilesAnalyzed")
        }) {
            @Override
            public boolean isCellEditable(int row, int column) {
                return false;
            }
        };
        final JTable table = new JTable(tableModel);

        frame = new JFrame("Dedupe");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Add the ubiquitous "Hello World" label.
//        JLabel label = new JLabel("Hello World");
//        frame.getContentPane().add(label);

        menuBar = new JMenuBar();

        menu = new JMenu(getString("File"));
        menuItem = new JMenuItem(getString("AddDirectory"), KeyEvent.VK_A);
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                final JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int returnVal = chooser.showOpenDialog(frame);
                if(returnVal == JFileChooser.APPROVE_OPTION) {
                    File dir = chooser.getSelectedFile();
                    if(!dir.isDirectory())
                        throw new RuntimeException("That isn't a directory");
                    if(!DAO.INST.addDir(dir)) {
                        JOptionPane.showMessageDialog(null, "Unable to add that folder");
                        return;
                    }
                    refreshTable(table, tableModel, actionMenu);
                }
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);

        actionMenu.setEnabled(false);
        menuBar.add(actionMenu);

        menu = new JMenu(getString("Help"));
        menuItem = new JMenuItem(getString("About"));
        menuItem.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JDialog dg = new HelpDialog(frame);
                dg.setLocationRelativeTo(frame);
                dg.setVisible(true);
            }
        });
        menu.add(menuItem);
        menuBar.add(menu);


        frame.setJMenuBar(menuBar);

        scrollPane = new JScrollPane(table);
        table.setFillsViewportHeight(true);

        frame.add(scrollPane);


        //Display the window.
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);

        refreshTable(table, tableModel, actionMenu);
    }

    private static ListSelectionListener lstr;
    private static void refreshTable(final JTable table, final DefaultTableModel tableModel, final JMenu actionMenu) {
        refreshTable(table, tableModel, actionMenu, -1);
    }
    private static void refreshTable(final JTable table, final DefaultTableModel tableModel, final JMenu actionMenu, int preselect) {

        final java.util.List<Dir> dirs = DAO.INST.getDirs();
        while(tableModel.getRowCount() > 0) {
            tableModel.removeRow(0);
        }

        for(Dir dir : dirs) {
            tableModel.addRow(new Object[] {
                    dir.getPath(),
                    dir.getFileCount() == -1 ? getString("unknown") : dir.getFileCount(),
                    dir.getDuplicates() == -1 ? getString("unknown") : dir.getDuplicates(),
                    dir.getFileCount() > 0 ? dir.getAnalyzed()+"/"+dir.getFileCount()+" "+(rnd((((double) dir.getAnalyzed()) / ((double) dir.getFileCount()))*100D)+"%") : ""
            });
        }

        if(lstr != null)
            table.getSelectionModel().removeListSelectionListener(lstr);

        table.getSelectionModel().addListSelectionListener(lstr = new ListSelectionListener() {
            @Override
            public void valueChanged(ListSelectionEvent e) {
                ListSelectionModel lsm = (ListSelectionModel)e.getSource();

                actionMenu.setEnabled(false);
                actionMenu.removeAll();

                if((!lsm.getValueIsAdjusting()) && table.getSelectedRow() != -1
                        && lsm.getMaxSelectionIndex() == lsm.getMinSelectionIndex()) {

                    final Dir selectedDir = dirs.get(table.getSelectedRow());

                    actionMenu.setEnabled(true);

                    JMenuItem openMenu = new JMenuItem(getString("Open"));
                    JMenuItem manageIgnores = new JMenuItem(getString("ManageIgnores"));
                    JMenuItem analyzeMenu = new JMenuItem(getString("Analyze"));
                    JMenuItem dedupeMenu = new JMenuItem(getString("Delete Duplicates"));
                    JMenuItem cleanMenu = new JMenuItem(getString("Clean"));
                    JMenuItem deleteMenu = new JMenuItem(getString("Delete"));

                    dedupeMenu.setEnabled(selectedDir.getDuplicates() > 0);

                    openMenu.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            try {
                                Desktop.getDesktop().open(selectedDir.getFile());
                            } catch (IOException e1) {
                                throw new RuntimeException(e1);
                            }
                        }
                    });
                    manageIgnores.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            JDialog dialog = new ManageIgnoresDialog(frame, selectedDir, dirs);
                            dialog.setVisible(true);
                        }
                    });
                    analyzeMenu.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {

                            final Util.TypeBox box = new Util.TypeBox(false);
                            final Util.TypeBox box1 = new Util.TypeBox();

                            final JDialog dialog = new JDialog(frame);
                            dialog.setModal(true);
                            dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                            dialog.setTitle(getString("Analyzing"));

                            GridBagConstraints c = new GridBagConstraints();

                            JPanel pane = new JPanel(new GridBagLayout());
                            pane.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));

                            JLabel label = new JLabel(getString("Analyzing Directory"));
                            c.anchor = GridBagConstraints.PAGE_START;
                            c.gridy = 0;
                            pane.add(label, c);

                            final JLabel dname = new JLabel(selectedDir.getPath(), JLabel.CENTER);
                            c.gridy = 1;
                            pane.add(dname, c);

                            final JLabel statusText = new JLabel(" ", JLabel.CENTER);
                            c.gridy = 2;
                            c.anchor = GridBagConstraints.CENTER;
                            c.weighty = .30;
                            pane.add(statusText, c);

                            final JLabel dupeText = new JLabel(getString("Duplicates")+" "+0, JLabel.CENTER);
                            c.gridy = 3;
                            c.anchor = GridBagConstraints.CENTER;
                            c.weighty = .20;
                            pane.add(dupeText, c);

                            c.gridy = 4;
                            c.weighty = .30;
                            c.weightx = .75;
                            final JLabel fileNameLabel = new JLabel(" ", JLabel.CENTER);
                            pane.add(fileNameLabel, c);

                            final JButton cancelButton = new JButton(getString("Cancel"));
                            cancelButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    box.booleanVal = true;
                                    cancelButton.setText(getString("Cancelling"));
                                    cancelButton.setEnabled(false);
                                }
                            });
                            c.gridy = 5;
                            c.anchor = GridBagConstraints.PAGE_END;
                            pane.add(cancelButton, c);

                            dialog.add(pane);

                            dialog.setSize(300, 200);


                            new Scanjob(selectedDir, new Scanjob.ScanHandler() {
                                @Override
                                public void beforeStart() {
                                    box.intVal = 0;
                                    box1.intVal = 0;
                                }
                                @Override
                                public void onVisitFile(Scanjob job, final Path p, final int currentFile, final int totalFiles) {
                                    selectedDir.setFileCount(totalFiles);
                                    selectedDir.setAnalyzed(++box1.intVal);
                                    if(box.booleanVal) {
                                        job.cancel();
                                        return;
                                    }
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            dname.setText(Files.canonicalPath(p.toFile().getParentFile()));
                                            statusText.setText("File "+currentFile+" of "+totalFiles);
                                            fileNameLabel.setText(p.toFile().getName());
                                        }
                                    });
                                }
                                @Override
                                public void onFinish() {
                                    selectedDir.setDuplicates(box.intVal);
                                    selectedDir.setFileCount(selectedDir.getFileCount() == -1 ? 0 : selectedDir.getFileCount());
                                    DAO.INST.serialize(dirs);
                                    dialog.dispose();
                                    SwingUtilities.invokeLater(new Runnable() {
                                        @Override
                                        public void run() {
                                            refreshTable(table, tableModel, actionMenu, table.getSelectedRow());
                                        }
                                    });
                                }
                                @Override
                                public boolean isCanceled() {
                                    return false;
                                }
                                @Override
                                public void onFoundDupe() {
                                    box.intVal++;
                                    dupeText.setText(getString("Duplicates")+" "+box.intVal);
                                }
                            }).start();

                            dialog.setLocationRelativeTo(frame);
                            dialog.setVisible(true);
                        }
                    });
                    dedupeMenu.addActionListener(new ActionListener() {
                        private List<File> doomedFolders = new ArrayList<>();
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            walk();
                        }
                        void walk() {
                            new Thread() {
                                @Override
                                public void run() {
                                    final Path dupFolder = new File(Scanjob.dirBase(selectedDir), "dups").toPath();
                                    try {
                                        java.nio.file.Files.walkFileTree(dupFolder, new SimpleFileVisitor<Path>() {
                                            String isDoomed(String[] dups) {
                                                for(String dup : dups) {
                                                    for(File doomed : doomedFolders)
//                                                        if(Files.canonicalPath(new File(dup).getParentFile()).equals(doomed))
//                                                            return dup;
                                                    if(Files.isDescendantOf(doomed, new File(dup)))
                                                        return dup;
                                                }
                                                return null;
                                            }
                                            @Override
                                            public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {

                                                File indexFile = Scanjob.indexPath(selectedDir, file.getFileName().toString());
                                                final String[] dups = Files.getContents(indexFile).trim().split("\n");
                                                PickerDialog.WalkResult result;

                                                String doomedDup = isDoomed(dups);
                                                if (doomedDup != null) {
                                                    result = new PickerDialog.WalkResult();
                                                    result.delete = new File(doomedDup);
                                                } else
                                                    result = bringPicker(dups);

                                                if(result.cancelSkip != null && result.cancelSkip) {
                                                    walk();
                                                    return FileVisitResult.TERMINATE;
                                                } if(result.skip != null && result.skip) {
                                                    return FileVisitResult.CONTINUE;
                                                } else if(result.cancel != null && result.cancel) {
                                                    finish();
                                                    return FileVisitResult.TERMINATE;
                                                } else if(result.delete != null) {
                                                    boolean deleted = result.delete.delete();
                                                    if(!deleted) {
                                                        //int res = JOptionPane.showConfirmDialog(frame, getString("DeleteFail"), null, JOptionPane.OK_OPTION);
                                                        JOptionPane.showMessageDialog(frame, getString("DeleteFail"));
                                                        walk();
                                                        return FileVisitResult.TERMINATE;
                                                    }

                                                    if(result.deleteAllFilesInParent) {
                                                        doomedFolders.add(result.delete.getParentFile());
                                                    }

                                                    selectedDir.setDuplicates(selectedDir.getDuplicates()-1);
                                                    DAO.INST.serialize(dirs);
                                                    SwingUtilities.invokeLater(new Runnable() {
                                                        @Override
                                                        public void run() {
                                                            refreshTable(table, tableModel, actionMenu);
                                                        }
                                                    });

                                                    if(dups.length == 2) {
                                                        file.toFile().delete();
                                                        indexFile.delete();
                                                    } else {
                                                        String duplist = "";
                                                        for(int i=0; i < dups.length; i++) {
                                                            if(dups[i].equals(Files.canonicalPath(result.delete)))
                                                                continue;
                                                            duplist += dups[i]+"\n";
                                                        }
                                                        Put.contents(indexFile, duplist);
                                                        walk();
                                                        return FileVisitResult.TERMINATE;
                                                    }


                                                    return FileVisitResult.CONTINUE;
                                                }

                                                throw new RuntimeException("unexpected state");
                                            }
                                            @Override
                                            public FileVisitResult postVisitDirectory(Path scandir, IOException e) {
                                                if(scandir.equals(dupFolder)) {
                                                    finish();
                                                }
                                                return FileVisitResult.CONTINUE;
                                            }

                                            void finish() {
                                                doomedFolders = new ArrayList<>();
                                            }
                                        });
                                    } catch (IOException e1) {
                                        throw new RuntimeException(e1);
                                    }
                                }
                                PickerDialog.WalkResult bringPicker(final String[] dups) {
                                    final PickerDialog.WalkResult result = new PickerDialog.WalkResult();

                                    SwingUtilities.invokeLater(new Runnable() {

                                        @Override
                                        public void run() {
                                            JDialog picker = new PickerDialog(frame, selectedDir, dups, result);
                                            picker.setVisible(true);
                                        }

                                        private void fit(final JDialog dialog) {
                                            if(dialog.getWidth() < 300)
                                                dialog.setSize(300, dialog.getHeight());
                                            if(dialog.getHeight() < 200)
                                                dialog.setSize(dialog.getWidth(), 200);
                                        }
                                    });

                                    while(!result.haveResults()) {
                                        sleepnow(10);
                                    }

                                    return result;
                                }
                                void sleepnow(long ms) {
                                    try {
                                        Thread.sleep(ms);
                                    } catch (InterruptedException e1) {
                                        throw new RuntimeException(e1);
                                    }
                                }
                            }.start();
                        }
                    });
                    deleteMenu.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {
                            int choice = JOptionPane.showConfirmDialog(frame, getString("DeleteConfirm"), null, JOptionPane.YES_NO_OPTION);
                            if(choice != 0) return;
                            dirs.remove(table.getSelectedRow());
                            DAO.INST.serialize(dirs);
                            SwingUtilities.invokeLater(new Runnable() {
                                @Override
                                public void run() {
                                    refreshTable(table, tableModel, actionMenu);
                                }
                            });
                        }
                    });
                    cleanMenu.addActionListener(new ActionListener() {
                        @Override
                        public void actionPerformed(ActionEvent e) {

                            final Util.TypeBox box = new Util.TypeBox(false);

                            final JDialog dialog = new JDialog(frame);
                            dialog.setModal(true);
                            dialog.setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
                            dialog.setTitle(getString("Cleaning"));

                            JPanel panel = new JPanel(new GridBagLayout());
                            panel.setBorder(BorderFactory.createEmptyBorder(5,5,5,5));
                            GridBagConstraints c = new GridBagConstraints();

                            JLabel label = new JLabel(getString("Cleaning Directory"));
                            c.gridy = 0;
                            c.anchor = GridBagConstraints.PAGE_START;
                            panel.add(label, c);

                            final JLabel dirLabel = new JLabel(selectedDir.getPath());
                            c.gridy = 1;
                            panel.add(dirLabel, c);

                            final JLabel fileStatus = new JLabel(" - ");
                            c.gridy = 2;
                            c.anchor = GridBagConstraints.CENTER;
                            c.weighty = .5;
                            c.weightx = .75;
                            panel.add(fileStatus, c);

                            final JButton cancelButton = new JButton(getString("Cancel"));
                            c.gridy = 3;
                            c.anchor = GridBagConstraints.PAGE_END;
                            c.weighty = .5;
                            panel.add(cancelButton, c);

                            cancelButton.addActionListener(new ActionListener() {
                                @Override
                                public void actionPerformed(ActionEvent e) {
                                    cancelButton.setEnabled(false);
                                    cancelButton.setText(getString("Cancelling"));
                                    box.booleanVal = true;
                                }
                            });

                            dialog.add(panel);

                            new Thread() {
                                @Override
                                public void run() {
                                    cleanup(selectedDir, new Scanjob.ScanHandler() {
                                        @Override
                                        public void onVisitFile(Scanjob job, final Path p, int currentFile, int totalFiles) {
                                            SwingUtilities.invokeLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    fileStatus.setText(p.toFile().getName());
                                                }
                                            });
                                        }
                                        @Override
                                        public void onFinish() {
                                            selectedDir.setDuplicates(-1);
                                            selectedDir.setFileCount(-1);
                                            DAO.INST.serialize(dirs);
                                            dialog.dispose();
                                            SwingUtilities.invokeLater(new Runnable() {
                                                @Override
                                                public void run() {
                                                    refreshTable(table, tableModel, actionMenu, table.getSelectedRow());
                                                }
                                            });
                                        }
                                        @Override
                                        public boolean isCanceled() {
                                            return box.booleanVal;
                                        }
                                    });
                                }
                            }.start();

                            dialog.setSize(200,150);
                            dialog.setLocationRelativeTo(frame);
                            dialog.setVisible(true);
                        }
                    });

                    actionMenu.add(openMenu);
                    actionMenu.add(manageIgnores);
                    actionMenu.add(analyzeMenu);
                    actionMenu.add(dedupeMenu);
                    actionMenu.add(cleanMenu);
                    actionMenu.add(deleteMenu);
                }
            }
        });

        if(preselect != -1)
            table.setRowSelectionInterval(preselect, preselect);
    }

    private static void cleanup(Dir dir, final Scanjob.ScanHandler handler) {
        File[] dirs = new File[] {
                Scanjob.dirBase(dir)
        };
        for(int i=0; i < dirs.length; i++) {
            if(!dirs[i].exists()) continue;
            try {
                java.nio.file.Files.walkFileTree(dirs[i].toPath(), new SimpleFileVisitor<Path>() {
                    @Override
                    public FileVisitResult visitFile(Path file, BasicFileAttributes attr) {
                        if(handler.isCanceled()) {
                            return FileVisitResult.TERMINATE;
                        }
                        handler.onVisitFile(null, file, 0, 0);
                        file.toFile().delete();
                        return FileVisitResult.CONTINUE;
                    }
                    @Override
                    public FileVisitResult postVisitDirectory(Path dir, IOException exc) throws IOException {
                        handler.onVisitFile(null, dir, 0, 0);
                        dir.toFile().delete();
                        return FileVisitResult.CONTINUE;
                    }
                });
            } catch (IOException e1) {
                throw new RuntimeException(e1);
            }
        }

        handler.onFinish();
    }

    public static void main(String[] args) {
        //Schedule a job for the event-dispatching thread:
        //creating and showing this application's GUI.
        javax.swing.SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                createAndShowGUI();
            }
        });
    }

    public static double rnd(double d) {
        return Math.round(d*100D)/100D;
    }
}

